#include<vector>
#include<iostream>
#include<fstream>
#include"item.h"
#include"input_output.h"

#pragma once

using namespace std;

class ToDoList{
    private:
        vector<ItemList> aList;
        const char* apFileName = "Data.txt";
    public:
        vector<ItemList> getList();
        void Add();
        void Update(int id);
        void Filter(int choice);
        void Remove(int id);
        void SaveData();
        void LoadData();
};
