#include<iostream>
#include<vector>
#include"item.h"

#pragma once

using namespace std;

class IO
{
    public:
        void Print(vector<ItemList> list);
        string BoolToString(bool Boolean);
        int GetInt();
        void Writing(string str);
};