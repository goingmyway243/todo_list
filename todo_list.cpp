#include"todo_list.h"

vector<ItemList> ToDoList::getList()
{
    return this->aList;
}

void ToDoList::Add()
{
    string str;
    getline(cin,str);
    cin.clear();
    getline(cin,str);    
    ItemList tmp = ItemList(str,false);
    aList.push_back(tmp);
}

void ToDoList::Update(int id)
{
    if(id>=0 && id<this->aList.size())
    {
        this->aList[id].setDone(true);
    }
}

void ToDoList::Filter(int choice)
{
    vector<ItemList> tmp;
    bool filter;
    IO* io = new IO();
            
    if(choice == 1 || choice == 2)
    {
        if(choice == 1)
        {
            filter = true;
        }
        else
        {
            filter = false;
        }
                
        for(int i=0;i<this->aList.size();i++)
        {
            if(this->aList[i].getDone() == filter)
            {
                tmp.push_back(this->aList[i]);
            }
        }
        io->Writing("\n======= List after filter ======\n");
        io->Print(tmp);
        io->Writing("\n");
    }
    else
    {
        io->Writing("Error, nothing happened!\n");
    }
    system("pause");
}

void ToDoList::Remove(int id)
{
    if(id>=0 && id<this->aList.size())
    {
        this->aList.erase(this->aList.begin() + id);
    }
}

void ToDoList::SaveData()
{
    fstream file;
    file.open(this->apFileName,ios::out);
            
    file<<this->aList.size()<<endl;
    for(int i=0;i<this->aList.size();i++)
    {
        file<<this->aList[i].getContent()<<endl;
        file<<this->aList[i].getDone()<<endl;
    }

    file.close();
}

void ToDoList::LoadData()
{
    ItemList tmp;
    string str;
    bool isDone;
    int n;
    fstream file;
    IO io;
            
    this->aList.clear();
    file.open(this->apFileName,ios::in);
			
	if(file.fail())
	{
		return;
	}
			
	file>>n;
	getline(file,str);
	for(int i=0;i<n;i++)
	{
	    getline(file,str);
	    tmp.setContent(str);
	    getline(file,str);
        if(str == "1")
        {
            isDone = true;
        }
        else
        {
            isDone = false;
        }
        
	    tmp.setDone(isDone);
	    this->aList.push_back(tmp);
	}
            
    file.close();
}