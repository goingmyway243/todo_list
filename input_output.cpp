#include"input_output.h"

    
void IO::Print(vector<ItemList> list)
{
    if(list.size()>0)
    {
        for(int i=0;i<list.size();i++)
        {
            cout<<"index "<<i<<": "<<list[i].getContent()<<" -- "<<this->BoolToString(list[i].getDone())<<endl;
        }
    }
    else
    {
        cout<<"//Nothing in lists";
    }   
}

string IO::BoolToString(bool Boolean)
{
    if(Boolean == true)
    {
        return "Yes";
    }
    else
    {
        return "No";
    }
}

int IO::GetInt()
{
    int id;
    cin>>id;
    return id;
}

void IO::Writing(string str)
{
    cout<<str;
}

