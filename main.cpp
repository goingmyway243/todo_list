#include"todo_list.h"

using namespace std;

int main()
{
    int choice;
    bool dk=true;
    ToDoList main_list;
    IO main_io;
   	main_list.LoadData();

    while (dk)
    {
        cout<<"======== To Do List ======="<<endl;
        main_io.Print(main_list.getList());
        cout<<"\n=========== Menu =========="<<endl;
        cout<<"1. Add"<<endl;
        cout<<"2. Update"<<endl;
        cout<<"3. Filter"<<endl;
        cout<<"4. Remove"<<endl;
        cout<<"0. Exit"<<endl;
        cout<<"==========================="<<endl;
        cout<<"Your choice: ";
        cin.clear();
        cin>>choice;

        switch(choice)
        {
            case 1:
            {
                main_io.Writing("Task name: ");
                main_list.Add();
                break;
            }
            case 2:
            {
                main_io.Writing("Which index: ");
                int id = main_io.GetInt();
                main_list.Update(id);
                break;
            }
            case 3:
            {
                main_io.Writing("1. Yes\t2.No\nYour choice: ");
                int choice = main_io.GetInt();
                main_list.Filter(choice);
                break;
            }
            case 4:
            {
                main_io.Writing("Which index: ");
                int id = main_io.GetInt();
                main_list.Remove(id);
                break;
            }
            case 0:
            {
                dk = false;
                break;
            }
            default:
            {
                cout<<"Error!"<<endl;
                break;
            }
            
        }

        system("cls");
    }
    
    main_list.SaveData();
    return 0;
}
