#include<string>

#pragma once

using namespace std;

class ItemList
{
    private: 
        string aContent;
        bool aIsDone;
    public:
        ItemList();
        ItemList(string str, bool isDone);
        string getContent();
        void setContent(string str);
        bool getDone();
        void setDone(bool isDone);
};