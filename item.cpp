#include"item.h"

ItemList::ItemList()
{
    this->aContent = "";
    this->aIsDone = false;
}
ItemList::ItemList(string str, bool isDone)
{
    this->aContent = str;
    this->aIsDone = isDone;
}

string ItemList::getContent()
{
    return this->aContent;
}
void ItemList::setContent(string str)
{
    this->aContent=str;
}

bool ItemList::getDone()
{
    return this->aIsDone;
}
void ItemList::setDone(bool isDone)
{
    this->aIsDone=isDone;
}